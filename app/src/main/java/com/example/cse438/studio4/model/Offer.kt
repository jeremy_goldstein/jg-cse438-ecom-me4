package com.example.cse438.studio4.model

import java.io.Serializable

class Offer(): Serializable {
    private var id: String = ""
    private var currency: String = ""
    private var firstrecorded_at: Long = 0
    private var price: Double = 0.0
    private var lastrecorded_at: Long = 0
    private var seller: String = ""
    private var condition: String = ""
    private var availability: String = ""
    private var isactive: Int = 0

    constructor(
        id: String = "",
        currency: String = "",
        firstrecorded_at: Long = 0,
        price: Double = 0.0,
        lastrecorded_at: Long = 0,
        seller: String = "",
        condition: String = "",
        availability: String = "",
        isactive: Int = 0
    ): this() {
        this.id = id
        this.currency = currency
        this.firstrecorded_at = firstrecorded_at
        this.price = price
        this.lastrecorded_at = lastrecorded_at
        this.seller = seller
        this.condition = condition
        this.availability = availability
        this.isactive = isactive
    }

    //                        //
    // ------ Getters ------- //
    //                        //

    fun getId(): String {
        return this.id
    }

    fun getCurrency(): String {
        return this.currency
    }

    fun getFirstRecordedAt(): Long {
        return this.firstrecorded_at
    }

    fun getPrice(): Double {
        return this.price
    }

    fun getLastRecordedAt(): Long {
        return this.lastrecorded_at
    }

    fun getSeller(): String {
        return this.seller
    }

    fun getCondition(): String {
        return this.condition
    }

    fun getAvailability(): String {
        return this.availability
    }

    fun getIsActive(): Int {
        return this.isactive
    }

//    fun getActiveState(): Boolean {
//        return this.isactive > 0
//    }
}